<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tags')->insert([
            'name' => 'Código',
        ]);

        DB::table('tags')->insert([
            'name' => 'Idiomas',
        ]);

        DB::table('tags')->insert([
            'name' => 'PHP',
        ]);

        DB::table('tags')->insert([
            'name' => 'Informática',
        ]);

        DB::table('tags')->insert([
            'name' => 'Finanzas',
        ]);

        DB::table('tags')->insert([
            'name' => 'Laravel',
        ]);
    }
}
