<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class PhrasesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('phrases')->insert([
            'phrase' => '  La ambición es el camino al éxito. La persistencia es el vehículo en el que llegas.',
            'author' => 'Bill Bradley',
            'biography' => 'http://es.wikipedia.org/wiki/Bill_Bradley'
        ]);

        DB::table('phrases')->insert([
            'phrase' => 'La energía y la persistencia conquistan todas las cosas.',
            'author' => ' Benjamin Franklin',
            'biography' => 'http://es.wikipedia.org/wiki/Benjamin_Franklin'
        ]);

        DB::table('phrases')->insert([
            'phrase' => '  Los grandes espíritus siempre han encontrado una violenta oposición de parte de mentes mediocres.',
            'author' => ' Albert Einstein',
            'biography' => 'http://es.wikipedia.org/wiki/Albert_Einstein'
        ]);
    }
}
