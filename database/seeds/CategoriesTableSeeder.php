<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'name' => 'Educación',
        ]);

        DB::table('categories')->insert([
            'name' => 'Software',
        ]);

        DB::table('categories')->insert([
            'name' => 'Programación',
        ]);

        DB::table('categories')->insert([
            'name' => 'Off-topic',
        ]);

        DB::table('categories')->insert([
            'name' => 'Manualidades',
        ]);

        DB::table('categories')->insert([
            'name' => 'Videos',
        ]);
    }
}
