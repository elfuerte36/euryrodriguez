<?php

namespace euryrodriguez\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
      View::composer(['welcome'],'euryrodriguez\Http\ViewComposers\AsideComposer');
      View::composer(['index.article'],'euryrodriguez\Http\ViewComposers\AsideComposer');
      View::composer(['home'],'euryrodriguez\Http\ViewComposers\StadisticsHomeComposer');
     
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
