<?php

namespace euryrodriguez;

use Illuminate\Database\Eloquent\Model;

class Phrase extends Model
{
    protected $table = "phrases";

    protected $fillable = ['phrase','author','biography'];
}
