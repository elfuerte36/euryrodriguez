<?php

namespace euryrodriguez;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;


class Article extends Model
{
           use Sluggable;
           use SluggableScopeHelpers;
          /**
           * Return the sluggable configuration array for this model.
           *
           * @return array
           */
          public function sluggable()
          {
              return [
                  'slug' => [
                      'source' => 'title'
                  ]
              ];
          }
            
            protected  $table  = "articles";

            protected $fillable = ['title','content','category_id','user_id']; 

            public function category()
            {
            	return $this->belongsTo('euryrodriguez\Category');
            }

            public function user()
            {
            	return $this->belongsTo('euryrodriguez\User');
            }

            public function images()
            {
            	return $this->hasMany('euryrodriguez\Image');
            }

            public function tags()
            {
            	return $this->belongsToMany('euryrodriguez\Tag');
            }
            public function scopeSearch($query,$title)
            {
                return $query->where('title','LIKE',"%$title%");
            }
}
