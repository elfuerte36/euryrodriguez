<?php

namespace euryrodriguez\Http\ViewComposers;

use euryrodriguez\Tag;
use Illuminate\Contracts\View\View;
use euryrodriguez\Category;

class AsideComposer
{
    public function compose(View $view)
    {
        $categories = Category::orderBy('name','desc')->paginate(4);
        $Tags = \DB::table('tags')
            ->inRandomOrder()
            ->paginate(4);

        $view->with('categories',$categories)->with('tags',$Tags);
    }

}