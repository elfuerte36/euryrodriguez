<?php

namespace euryrodriguez\Http\ViewComposers;


use Illuminate\Contracts\View\View;
use euryrodriguez\Article;
use euryrodriguez\Tag;
use euryrodriguez\Category;
use euryrodriguez\Image;
use euryrodriguez\Phrase;
use euryrodriguez\User;

class StadisticsHomeComposer
{
    public function compose(View $view)
    {
        $Articles   = Article::where('user_id',\Auth::user()->id);
        $Tags       = Tag::all();
        $Categories = Category::all();
        $Images     = Image::all();
        $Phrases    = Phrase::all();
        $Users      = User::all();

      return $view->with('Articles',$Articles)
            ->with('Tags',$Tags)
            ->with('Categories',$Categories)
            ->with('Images',$Images)
            ->with('Phrases',$Phrases)
            ->with('Users',$Users);
    }

}