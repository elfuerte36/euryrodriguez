<?php
//RUTAS DEL FRONT-END

Route::get('/',[
	'as'   => 'front.index',
	'uses' => 'IndexController@index'
]);

Route::get('mail',[
    'uses' => 'MailController@store',
    'as'  => 'mail.store'
]);

Route::get('mail/{name}/{email}/{mensajeContact}','MailController@store');

Route::get('admin/login', 'Auth\AuthController@showLoginForm');

Route::get('categories/{name}',[
'uses' => 'IndexController@searchCategory',
 'as'  => 'index.search.category'
]);

Route::get('tags/{name}',[
    'uses' => 'IndexController@searchTag',
    'as'  => 'index.search.tag'
]);

Route::get('articles/{category}/{slug}',[
    'uses' => 'IndexController@ViewArticle',
    'as'   => 'index.view.article'
]);

/*Route::get('articles',[
'as' => 'articles',
'uses' => 'UserController@index'
]);*/

//RUTAS DEL PANEL DE ADMINISTRACIÓN.

Route::group(['prefix' => 'admin','middleware' => 'auth'], function() {

	Route::get('/',function(){
		return view('home');
	});


	Route::get('/home',function(){
		return view('home');
	});

	Route::group(['middleware'=>'admin'],function()
	{
		Route::resource('users', 'UsersController');
		Route::get('users/{id}/destroy',[
			'uses' => 'UsersController@destroy',
			'as'   => 'admin.users.destroy'
		]);
	});

	Route::resource('categories', 'CategoriesController');
    Route::get('categories/{id}/destroy',
    		[
    	'uses' => 'CategoriesController@destroy',
    	'as' => 'admin.categories.destroy'

    		]);
	Route::resource('tags', 'TagsController');
	Route::get('tags/{id}/destroy',
		[
			'uses' => 'TagsController@destroy',
			'as' => 'admin.tags.destroy'

		]);

	Route::resource('phrases', 'PhrasesController');
	Route::get('phrases/{id}/destroy',
		[
			'uses' => 'PhrasesController@destroy',
			'as' => 'admin.phrases.destroy'

		]);

	Route::resource('articles', 'ArticlesController');
	Route::get('articles/{id}/destroy',
		[
			'uses' => 'ArticlesController@destroy',
			'as' => 'admin.articles.destroy'

		]);

	Route::get('welcome',[
		'uses' => 'HomeController@index',
		'as'   => 'admin.home.index'
	]);

	Route::get('Images',[
	'uses' => 'ImagesController@index',
	'as'   => 'admin.images.index'
	]);

});

// Authentication Routes...
Route::get('/admin/login', 'Auth\AuthController@showLoginForm');
Route::post('/admin/login', 'Auth\AuthController@login');
Route::get('/admin/logout', 'Auth\AuthController@logout');

// Password Reset Routes...
Route::get('/admin/password/reset/{token?}', 'Auth\PasswordController@showResetForm');
Route::post('/admin/password/email', 'Auth\PasswordController@sendResetLinkEmail');
Route::post('/admin/password/reset', 'Auth\PasswordController@reset');
