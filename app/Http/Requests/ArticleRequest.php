<?php

namespace euryrodriguez\Http\Requests;

use euryrodriguez\Http\Requests\Request;

class ArticleRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'       =>'min:10|max:150|required|unique:articles',
            'content'     =>'min:60|required',
            'category_id' =>'required',
            'image'       =>'image|required'
        ];
    }
}
