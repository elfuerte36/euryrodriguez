<?php

namespace euryrodriguez\Http\Requests;

use euryrodriguez\Http\Requests\Request;

class UserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'min:4|max:120|required',
            'email'=> 'min:15|max:125|required|uniqued:users',
            'password'=> 'min:4|max:120|required'
        ];
    }
}
