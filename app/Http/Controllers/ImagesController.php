<?php

namespace euryrodriguez\Http\Controllers;

use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use euryrodriguez\Image;
use Illuminate\Support\Facades\Redirect;
use euryrodriguez\Http\Requests;

class ImagesController extends Controller
{
    public function index()
    {
       $images = Image::orderBy('id','DESC')->paginate(5);
       $images->each(function($images){
           $images->article;
       });
        
       return view('admin.images.index')->with('images',$images);
    }
}
