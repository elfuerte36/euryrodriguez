<?php

namespace euryrodriguez\Http\Controllers;

use euryrodriguez\Http\Requests;
use Illuminate\Http\Request;
use euryrodriguez\Article;
use euryrodriguez\Tag;
use euryrodriguez\Category;
use euryrodriguez\Image;
use euryrodriguez\Phrase;
use euryrodriguez\User;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('home');
    }
}
