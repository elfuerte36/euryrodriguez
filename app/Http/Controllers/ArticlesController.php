<?php

namespace euryrodriguez\Http\Controllers;

use Illuminate\Http\Request;
use euryrodriguez\Http\Requests;
use euryrodriguez\Article;
use Laracasts\Flash\Flash;
use euryrodriguez\Category;
use euryrodriguez\Tag;
use euryrodriguez\Image;
use Illuminate\Support\Facades\Redirect;
use euryrodriguez\Http\Requests\ArticleRequest;

class ArticlesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
     $Articles = Article::Search($request->title)->orderBy('id','DESC')->paginate(5);
     $Articles->each(function($Articles)
     {
         $Articles->category;
         $Articles->user;
     });
        return view('admin.articles.index')->with('Articles',$Articles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $Categories = Category::orderBy('name','ASC')->lists('name','id');
        $Tags = Tag::orderBy('name','ASC')->lists('name','id');

        return view('admin.articles.create')
        ->with('Categories',$Categories)
        ->with('Tags',$Tags);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArticleRequest $request)
    {
            $nameIMG='';
            //Manipulacion de imagenes
            if($request->file('image'))
            {
                $file = $request->file('image');
                $nameIMG = 'euryrodriguez_'.time().'.'.$file->getClientOriginalExtension();
                $path = public_path().'/uploads/images/';
                $file->move($path,$nameIMG);
            }

            $Article = new Article($request->all());
            $Article->user_id = \Auth::user()->id;
            $Article->save();

            $Article->tags()->sync($request->tags);

            $Image = new Image();
            $Image->name = $nameIMG;
            $Image->article()->associate($Article);
            $Image->save();

        flash('El artículo '.substr($Article->title,0,15).' ha sido creado de forma satisfactoria!','success');
        return redirect()->route('admin.articles.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Categories = Category::orderBy('name','DESC')->lists('name','id');
        $Article   = Article::find($id);
        $tags = Tag::orderBy('name','DESC')->lists('name','id');
        $my_tags = $Article->tags->lists('id')->ToArray();

        return view('admin.articles.edit')
        ->with('categories',$Categories)
        ->with('article',$Article)
        ->with('tags',$tags)
        ->with('my_tags',$my_tags);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $Article = Article::find($id);
      $Article->fill($request->all());
      $Article->tags()->sync($request->tags);
      $Article->save();
      flash('El artículo '.substr($Article->title,0,15).' ha sido actualizado de forma satisfactoria!','warning');
      return redirect()->route('admin.articles.index');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $Article = Article::find($id);
       $Article->delete();
       flash('El artículo '.substr($Article->title,0,15).' ha sido eliminado de forma satisfactoria!','warning');
       return redirect()->route('admin.articles.index');

    }
}
