<?php

namespace euryrodriguez\Http\Controllers;

use Illuminate\Http\Request;
use euryrodriguez\Phrase;
use euryrodriguez\Http\Requests;
use Laracasts\Flash\Flash;

class PhrasesController extends Controller
{

    public function index()
    {
      $Phrases = Phrase::orderBy("id","ASC")->paginate(5);

      return view('admin.phrases.index')->with('Phrases',$Phrases);
    }

    public function store(Request $request)
    {
         $this->validate($request,[
             'phrase' => ['required','min:10'],
             'author' => ['required','max:50','min:3'],
             'biography' => ['required','min:10']
         ]);

        $Phrase = new Phrase($request->all());
        $Phrase->save();
        flash("La frase de  ".$Phrase->author." ha sido registrada de forma exitosa!", "success");
        return redirect()->route('admin.phrases.index');

    }

    public function create()
    {
       return view('admin.phrases.create');
    }

    public function show()
    {

    }

    public function edit($id)
    {
        $Phrase = Phrase::find($id);
        return view('admin.phrases.edit')->with('Phrase',$Phrase);
    }

    public function update(Request $request,$id)
    {
        $Phrase = Phrase::find($id);
        $Phrase->phrase = $request->phrase;
        $Phrase->author = $request->author;
        $Phrase->biography = $request->biography;
        $Phrase->save();
        flash("La frase de  ".$Phrase->author." ha sido actualizada de forma exitosa!", "success");
        return redirect()->route('admin.phrases.index');

    }

    public function destroy($id)
    {
        $Phrase = Phrase::find($id);
        $Phrase->delete();
        flash("La frase de  ".$Phrase->author." ha sido eliminada de forma exitosa!", "success");
        return redirect()->route('admin.phrases.index');
    }
}
