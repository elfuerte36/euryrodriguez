<?php

namespace euryrodriguez\Http\Controllers;

use Illuminate\Http\Request;
use euryrodriguez\Http\Requests;
use euryrodriguez\User;
use Laracasts\Flash\Flash;


class UsersController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $users = User::orderBy("id","ASC")->paginate(5);
       return view("admin.users.index")->with("users",$users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(request $request)
    {
        $this->validate($request,
            [
                'name' =>['required','max:120','min:4'],
                'email'=>['required','max:120','min:9','unique:users'],
                'password'=>['required','max:120','min:6']
            ]);

        $user = new User($request->all());
        $user->password = bcrypt($request->password);
        $user->save();
        flash("Se ha registrado ".$user->name." de forma exitosa!", "success");
        return redirect()->route("admin.users.index");

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	$user = User::find($id);
        return view('admin.users.edit')->with('user',$user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
          $user = User::find($id);
        //$user->fill($request->all()); sustituye los valores por los del request.
        $user->name  = $request->name;
        $user->email = $request->email;
        $user->type  = $request->type;
        $user->save(); 

        flash('El usuario '.$user->name.' ha sido editado de forma exitosa!','success');
        return redirect()->route('admin.users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $User = User::find($id);
        $User->delete();
        flash("Se ha eliminado el usuario ".$User->name." de forma exitosa!", "danger");
        return redirect()->route("admin.users.index");
 
    }
}
