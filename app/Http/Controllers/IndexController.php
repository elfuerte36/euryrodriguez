<?php

namespace euryrodriguez\Http\Controllers;

use euryrodriguez\Category;
use euryrodriguez\Tag;
use Illuminate\Http\Request;
use euryrodriguez\Http\Requests;
use euryrodriguez\Article;
use euryrodriguez\Phrase;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;

class IndexController extends Controller
{
    public function __construct()
    {
        Carbon::setLocale('es');
    }
    public function index(Request $request)
    {
        if(isset($_GET['term'])) {

            $Articles = Article::Search($request->title)->get();

?>
                <script type="text/javascript">

                    $("#Textbuscar").autocomplete({
                      source:[   <?php
                        for($i=0;$i<sizeof($Articles);$i++)
                        {

                            if(sizeof($Articles)-1==$i)
                            {
                                echo "{ label: '".$Articles[$i]['title']."', slug:'".$Articles[$i]['slug']."' , category: '".$this->getCategory($Articles[$i]['category_id'])."' }";

                            }else
                            {

                                echo "{ label: '".$Articles[$i]['title']."', slug:'".$Articles[$i]['slug']."' , category: '".$this->getCategory($Articles[$i]['category_id'])."' }".',';
                            }
                        }
                     ?>],
                            select: function( event, ui )
                            {
                            // alert(ui.item.category);

                            var raiz = "<?php echo URL::to('/');  ?>";
                            var url =raiz+'/articles/'+ui.item.category+'/'+ui.item.slug;
                            //alert(url);

                            window.location=url;
                }
                });

                </script>
          <?php

}else
        {
            $Articles = Article::Search($request->title)->orderBy('id', 'DESC')->paginate(6);
            $Articles->each(function ($Articles) {
                $Articles->category;
                $Articles->image;
            });

            $Phrase = DB::table('phrases')
                ->inRandomOrder()
                ->first();

            return view('welcome')->with('Articles', $Articles)->with('frase', $Phrase);

        }

        }
    public function searchCategory($name)
    {
        $Category = Category::SearchCategory($name)->first();
        $Articles = $Category->articles()->paginate(6);

        $Articles->each(function($Articles)
        {
            $Articles->category;
            $Articles->image;
        });

        $Phrase =  DB::table('phrases')
            ->inRandomOrder()
            ->first();

        return view('welcome')->with('Articles',$Articles)->with('frase',$Phrase);
    }

    public function searchTag($name)
    {
        $Tags = Tag::SearchTag($name)->first();
        $Articles = $Tags->articles()->paginate(6);

        $Articles->each(function($Articles)
        {
            $Articles->category;
            $Articles->image;
        });

        $Phrase =  DB::table('phrases')
            ->inRandomOrder()
            ->first();

        return view('welcome')->with('Articles',$Articles)->with('frase',$Phrase);
    }

    public function ViewArticle($category,$slug)
    {
        $Article = Article::findBySlugOrFail($slug);
        $Article->category;
        $Article->user;
        $Article->tag;
        $Article->image;

        $Phrase =  DB::table('phrases')
            ->inRandomOrder()
            ->first();

        return view('index.article')->with('Article',$Article)->with('frase',$Phrase);
    }

    public function getCategory($id)
    {
        $category = Category::find($id);
        return $category->name;
    }

}
