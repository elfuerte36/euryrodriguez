<?php

namespace euryrodriguez\Http\Controllers;

use Illuminate\Http\Request;
use euryrodriguez\Tag;
use euryrodriguez\Http\Requests;
use Laracasts\Flash\Flash;

class TagsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $Tags = Tag::search($request->name)->orderBy("id","ASC")->paginate(5);
        return view('admin.tags.index')->with('Tag',$Tags);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.tags.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(request $request)
    {
        $this->validate($request,[
            'name' => ['required','min:3','unique:tags']
        ]);

        $Tag = new Tag($request->all());
        $Tag->save();
        flash("La etiqueta ".$Tag->name." se ha agregado de forma exitosa!", "success");
        return redirect()->route("admin.tags.index");

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Tags = Tag::find($id);
        return view('admin.tags.edit')->with('Tag',$Tags);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Tag = Tag::find($id);
        $this->validate($request,[
            'name' => ['required','min:3']
        ]);
        $Tag->name = $request->name;
        $Tag->save();
        flash("La etiqueta ".$Tag->name." se ha actualizado  de forma exitosa!", "info");
        return redirect()->route("admin.tags.index");

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $Tag = Tag::find($id);
       $Tag->delete();
       flash("La etiqueta ".$Tag->name." se ha eliminado  de forma exitosa!", "danger");
       return redirect()->route("admin.tags.index");
    }
}
