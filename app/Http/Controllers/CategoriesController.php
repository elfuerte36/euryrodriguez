<?php

namespace euryrodriguez\Http\Controllers;

use Illuminate\Http\Request;

use euryrodriguez\Http\Requests;
use euryrodriguez\Category;
use Laracasts\Flash\Flash;

class CategoriesController extends Controller
{
       /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $categories = Category::orderBy('id','DESC')->paginate(5);

        return view('admin.categories.index')->with('categories',$categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $this->validate($request,
       [
           'name' => ['required','min:3','max:30','unique:categories']
       ]);

       $category = new Category();
       $NombreCategoria=$request->name;
       $category->name = $NombreCategoria;
       $category->save();
       flash('La categoria '.$NombreCategoria.' ha sido creada con exito!','success');
       return redirect()->route('admin.categories.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);
        return view('admin.categories.edit')->with('category',$category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Category = Category::find($id);
        $Category->name = $request->name;
        $Category->save();
        flash('La categoria '.$Category->name.' ha sido ','success');
        return redirect()->route('admin.categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);
        $category->delete();
        flash('La categoria '.$category->name.' ha sido eliminada correctamente.','success');
        return redirect()->route('admin.categories.index');

    }
}
