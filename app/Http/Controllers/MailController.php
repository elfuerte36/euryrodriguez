<?php

namespace euryrodriguez\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use euryrodriguez\Http\Requests;

class MailController extends Controller
{
    private $address;
    private $name;
    private $mensaje;
    private $to;
    private $content = array();
    private $asunto;

    public function store(Request $request)
    {
        $this->address = $request['email'];
        $this->name    = $request['name'];
        $this->asunto  = $request['asunto'];
        $this->mensaje = $request['mensajeContact'];
        $this->to      = 'eury958@gmail.com';
        $this->content = $request->all();
        $Respuesta ='';

        $Respuesta = Mail::send('emails.contact',$this->content,function($msj)
       {
           $msj->from($this->address,$this->name);
           $msj->subject($this->asunto);
           $msj->to($this->to);

       });
       if($Respuesta){

        echo " <script>

            var dialogo         = '#RespuestaAjax';
            var formname        = '#formname';
            var formemail       = '#formemail';
            var TextareaMessage = '#TextareaMessage';
            var asuntomensaje   = '#asuntomensaje';
            var Divprogressbar  = '#progressbar';

        $(dialogo).html('<b>El mensaje se ha enviado de forma satisfactoria. Gracias por escribirnos.</b>');
        $(dialogo).dialog({
               title: 'Mensaje Enviado',
               autoOpen:true, // no abrir automáticamente
               resizable: true, //permite cambiar el tamaño
               height:200, // altura
               width:300,
               modal: true, //capa principal, fondo opaco
               buttons: { //crear botón de cerrar
                'Cerrar': function() {
                    $( this ).dialog( 'close' );
                }
            }
        });
        //Limpiar los campos/*******************/
             $(formname).val('');/*******************/
             $(formemail).val('');/*******************/
             $(TextareaMessage).val('');/*******************/
             $(asuntomensaje).val('');/*******************/
             $('#Loading').html('');
             $('#FormularioContacto').dialog('close');
        //Limpiar los campos/*******************/
         $(Divprogressbar).progressbar({

             value:0

             }).next('div').html('<b>'+0+' de '+0+'</b>');

        </script>
        ";
       }else {
           echo "<script>
            alert('Los sentimos :( ha ocurrido un error durante el proceso, intente enviar el mensaje mas tarde.'); $('#FormularioContacto').dialog({ hide: { effect: 'explode', duration: 500 } });
             $(formname).val('');/*******************/
             $(formemail).val('');/*******************/
             $(TextareaMessage).val('');/*******************/
             $(asuntomensaje).val('');/*******************/
             $('#Loading').html('');
             $('#opacity').removeClass('formopacity');
            </script>";
       }
    }
}
