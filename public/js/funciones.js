﻿$(document).ready(function () {

    var TextBuscar = '#Textbuscar';
    var Dialogo = '#dialogo';
    var BtnBusqueda = '#btnbusqueda';
    var publicdirectory=$('#publicdirectory').val();


    $(window).scroll(function () {
        if ($(this).scrollTop() > 170) {
            $('#subir').fadeIn();
        } else {
            $('#subir').fadeOut();
        }
    });
//******************************************************************************
   /* $("#subir a").on("click", irA);

    function irA() {
        var seccion = $(this).attr('href');
        $("body,html").animate({
            scrollTop: $(seccion).offset().top
        }, 800);
        return false;
    }*/
    
//**********************************************************************************************//
    var InputActivarMenu = '#menu-bar';
    var BotonActivarMenu   = '#btnmenu';
    var menuactivo=true;

    $(InputActivarMenu).removeAttr('checked');

    $(BotonActivarMenu).click(function()
    {
        if(menuactivo)
        {
          $('.menu').css({'transform':'translateY(0%)'});
          menuactivo=false;

        }else
        {
          $('.menu').css({'transform':'translateX(-100%)'});
          menuactivo=true;
        }
    });

    
    
    $('.menu  a').hover(function () {
        $(this).animate({
            backgroundColor: "#DE740A"
        }, 200, function () {
        });
    }, function () {
        $(this).animate({
            backgroundColor: "#F93"
        }, 200, function () {
        });
    });

    var bxslider = $('.bxslider');

    if(bxslider.length){
        $(bxslider).bxSlider({
            auto:true,
            captions: true
        });
    }
    $('.linktitulo').tooltip();
    /***************Llamada automatica******************/
    var Form = $('#Form_buscar');
    var url  = Form.attr('action');
    var Data = Form.serialize();
    var Source = url+'/?'+Data+'&term=';
    BuscarArticulo(Form,url,Data,Source);
    /***************Llamada automatica******************/

    $(TextBuscar).keyup(function()
    {
        var Form = $('#Form_buscar');
        var url  = Form.attr('action');
        var Data = Form.serialize();
        var Source = url+'/?'+Data+'&term=';
        BuscarArticulo(Form,url,Data,Source);
    });
    function BuscarArticulo(Form,url,data,source)
    {
        $.get(source,data,function(result)
        {
            //alert(result);
            $('#resultadob').append(result);
        });
    }

     $(BtnBusqueda).click(function()
     {
         if($(TextBuscar).val()==0)
         {
             $(Dialogo).html("<b>Por favor escriba lo que quiera buscar</b>");

             //$(Dialogo).effect("shake","slow");
             $(Dialogo).dialog({
                 title: 'Campo de busqueda vacío!',
                 autoOpen:true, // no abrir automáticamente
                 resizable: true, //permite cambiar el tamaño
                 height:220, // altura
                 modal: true, //capa principal, fondo opaco
                 buttons: { //crear botón de cerrar
                     "Cerrar": function() {
                         $( this ).dialog( "close" );
                     }
                 }
             });
             $(Dialogo).dialog({ hide: { effect: "explode", duration: 500 } });
         }else
         {
             Form.submit();
         }
     });

 /*********************************************************************/
   /* var img;
    var div_foto='#photozoom';


    $("#blog img").hover(function()
    {

        $(div_foto).css({"display":"block"});
        photozoom($(this).attr('src'),$(div_foto));

    },function()
    {

        $(div_foto).css({"display":"none"});


    });
    */
    /**************************************************************************************/

    function photozoom(img,div){
        var imagen=img;
        $(div_foto).html("<img src='"+imagen+"' width='500' height='500' />");
    }

    var Divprogressbar = "#progressbar";
    var BtnMensaje = '#BtnMensaje';
    var TextareaMessage = '#TextareaMessage';

    if(BtnMensaje.length>0) {

        $(BtnMensaje).click(function (e) {
            e.preventDefault();


            var DivformContact = '#FormularioContacto';
            var FormHtml = '#formcontac';
            var urlAction = $(FormHtml).attr('action');
            var DataForm = $(FormHtml).serialize();
            var formname = '#formname';
            var formemail = '#formemail';
            var TextareaMessage = '#TextareaMessage';
            var asuntomensaje = '#asuntomensaje';

            $(DivformContact).dialog({
                title: 'Ponerse en contacto conmigo',
                autoOpen: true, // no abrir automáticamente
                resizable: true, //permite cambiar el tamaño
                modal: true, //capa principal, fondo opaco
                buttons: { //crear botón de cerrar
                    "Enviar": function () {

                        if ($(formname).val() == 0 || $(formemail).val() == 0 || $(TextareaMessage).val() == 0 || $(asuntomensaje).val() == 0) {
                            $(Dialogo).html("<b>Por favor escriba en los campos requeridos.</b>");
                            $(Dialogo).dialog({
                                title: 'Campos vacíos!',
                                autoOpen: true,
                                resizable: true,
                                width: 500,
                                height: 220,
                                modal: true,
                                buttons: {
                                    "Cerrar": function () {
                                        $(this).dialog("close");
                                    }
                                }
                            });
                        }
                        else {
                            /***************Enviar datos al controlador mail*******************************/
                            /*  $.get(urlAction,DataForm,function(result){$('#resultadob').append(result);});*/
                            $.ajax({
                                url: urlAction,
                                method: "GET",
                                dataType: "html",
                                async: true,
                                data: $('#formcontac').serialize(),
                                contentType: "application/x-www-form-urlencoded",
                                success: function (Resultado) {
                                    $('#resultadob').html(Resultado);
                                    $('#opacity').removeClass('formopacity');
                                    $(this).dialog("close");
                                },
                                beforeSend: function () {
                                    $('#Loading').html('<img src="' + publicdirectory + '/img/loading.gif" width="100" height="100" />');
                                    $('#opacity').addClass('formopacity')
                                },
                                error: function () {
                                    alert('Los sentimos :( ha ocurrido un error durante el proceso, intente enviar el mensaje mas tarde.');
                                    $('#opacity').removeClass('formopacity');
                                },
                                cache: false,
                                timeout: 14000

                            });
                            /****************************************************/
                        }

                    }, "Cancelar": function () {
                        $(this).dialog("close");
                    }
                }
            });
            $(DivformContact).dialog({hide: {effect: "explode", duration: 500}});
            $(FormHtml).css({'display': 'block'});
        });
    }

if($(TextareaMessage).length>0) {
    $(TextareaMessage).on("keyup", function () {
        //Cantidad de caracteres
        var maximo = 200;

        //texto del textarea

        var texto = $(this).val();

        //contador=cantidad de caracteres

        var cantidad = texto.length;

        var valor_actual = cantidad;

        //**********************************/

        if (cantidad <= maximo) {
            //Actualizar el valor de la barra

            $(Divprogressbar).progressbar({

                value: (cantidad / maximo) * 100

            }).next("div").html("<b>" + valor_actual + " de " + maximo + "");

        } else {
            $(this).val($(this).val().substr(0, maximo));
        }

    });

}

});