<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    @include('index.template.partials.metadatos')
    <title>@yield('title','Padevelopers')</title>
    
</head>
<body>
    @include('index.template.partials.header')
    <div id="frases" class="Comic_Sans">@yield('phrases')</div>
    @include('index.template.partials.slide')
    <div id="photozoom"></div>
    <div id="dialogo"></div>


         <section id="blog">
             @yield('content')
         </section>

@include('index.template.partials.footer')
</body>
</html>