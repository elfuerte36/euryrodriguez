<div id="categorias">
        <div>
    <h4 style="text-align: center;">Categor&iacute;as <span class="glyphicon glyphicon-th-list"></span></h4>

        <ul class="list-group">
           @foreach($categories as $category)
                <a href="{{ route('index.search.category',$category->name) }}" title="{{ $category->name }}" class="list-group-item active categorie linktitulo Comic_Sans">
                    <span class="badge">{{ $category->articles->count() }}</span>
                    <span class="glyphicon glyphicon-asterisk" aria-hidden="true"></span> {{ $category->name }}
                </a>
           @endforeach
        </ul>
        </div>
    <div>
        <ul class="list-group">
            <h4 style="text-align: center;">Etiquetas <span class="glyphicon glyphicon-tags"></span></h4>
            @foreach($tags as $tag)
                <a href="{{ route('index.search.tag',$tag->name) }}" title="{{ $tag->name }}" class="list-group-item tagslist  linktitulo Comic_Sans">
                    <span class="glyphicon glyphicon-asterisk" aria-hidden="true"></span> {{ $tag->name }}
                </a>
            @endforeach
        </ul>
    </div>
</div>


