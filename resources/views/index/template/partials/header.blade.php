@if(Auth::check())
@include('admin.template.partials.nav')
<style>
  #logo
  {
    margin-top: 120px !important;
  }
</style>
@endif
<header id="cabecera">
    <div class="contenedor">
      <a href="{{ URL::to('/') }}">
        <h1>
            <img src="{{ asset('img/dev.png') }}"/>
            Euryrodriguez
        </h1>
      </a>
    </div>
    <input type="checkbox" id="menu-bar"><label for="menu-bar"><i class="fa fa-bars" id="btnmenu"></i></label>
    <!--Buscador-->
    @include('index.template.partials.buscador')
    <!--Buscador-->
    <!--Menu-->
@include('index.template.partials.menu')
    <!--Menu-->

</header>
