 @if(App::environment('local'))

          <script src="{{ asset('plugins/jquery/js/jquery.2.1.3.min.js') }}"></script>
          <script type="text/javascript" src="{{ asset('plugins/bootstrap/js/bootstrap.min.js') }}"></script>
    
    @else
          <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
          <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script> 

    @endif

        <script src="{{ asset('js/funciones.js') }}"></script>

        <!-- bxSlider Javascript file -->
        <script src="{{ asset('plugins/bxslider/jquery.bxslider.min.js') }}"></script>
        <!-- bxSlider CSS file -->
        <link href="{{ asset('plugins/bxslider/jquery.bxslider.css') }}" rel="stylesheet" />

        <script type="text/javascript" src="{{ asset('plugins/Jquery-UI/jquery-ui.js') }}"></script>

    @if(App::environment('local'))

         <link rel="stylesheet" href="{{ asset('plugins/fonts/font-waesome/css/font-awesome.min.css') }}">
         <link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/bootstrap.css') }}">

    @else
        <!-- Fonts -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

        <!-- Styles -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

    @endif