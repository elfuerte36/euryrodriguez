 <footer>

    <div class="contenedor">

        <p class="copy">Eury Rodr&iacute;guez &copy; <?php echo date('Y');  ?></p>
        <div class="sociales">
            <a href="#">
                <i class="fa fa-facebook-official"  id="fb" aria-hidden="true"></i>
            </a>
            <a href="#">
                <i class="fa fa-twitter" id="tw" aria-hidden="true"></i>
            </a>
            <!--<a href="#">
                <i class="fa fa-instagram" aria-hidden="true"></i>
            </a>-->
            <a href="#">
                <i class="fa fa-youtube" id="yb" aria-hidden="true"></i>
            </a>
        </div>
    </div>

    <div id="pie_menu">
        <ul>

            @foreach($categories as $category)
                <li>
                    <a href="{{ route('index.search.category',$category->name) }}" class="linktitulo Comic_Sans" title="{{ $category->name }}">
                        {{ $category->name }}
                    </a>
                </li>
            @endforeach

        </ul>
    </div>
    </footer>