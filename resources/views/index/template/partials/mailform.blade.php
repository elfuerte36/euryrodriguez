<div id="RespuestaAjax"></div>
<div class="panel panel-primary" id="FormularioContacto" style="display: none;">

    <div class="panel-body">

        <div>
                {!! Form::open(['route'=>'mail.store','method'=>'GET','id'=>'formcontac','style'=>'display: none !important;']) !!}
                <div id="Loading"></div>
            <div id="opacity">
                <div class="form-group">
                    {!! Form::label('name','Nombre') !!}
                    {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Nombre Completo','id'=>'formname','required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('Asunto','Asunto') !!}
                    {!! Form::text('asunto',null,['class'=>'form-control','placeholder'=>'Asunto del mensaje','id'=>'asuntomensaje','required']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('email','Correo Electronico') !!}
                    {!! Form::email('email',null,['class'=>'form-control','placeholder'=>'example@gmail.com','id'=>'formemail','required'])!!}
                </div>

                <div class="form-group">
                    {!! Form::label('message','Mensaje') !!}
                    {!! Form::textarea('mensajeContact',null,['class'=>'form-control','id'=>'TextareaMessage','style'=>'height:130px','placeholder'=>'Escribe tu mensaje aquí, sea breve por favor','required'])!!}
                </div>
            </div>
                <div id="progressbar"></div>
                <div></div>

                <input type="hidden" id="publicdirectory" value="{{ URL::to('/') }}">
            {{ Form::close()  }}

        </div>

    </div>
</div>