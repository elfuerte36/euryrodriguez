<nav class="menu">

    
   <div>
          <a href="{{ URL::to('/') }}" title="Inicio de la pagína" class="linktitulo">
                  <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
                  Inicio</a>


        @foreach($categories as $category)

            <a href="{{ route('index.search.category',$category->name) }}" class="linktitulo" title="{{ $category->name }}">
                <span class="glyphicon glyphicon-asterisk" aria-hidden="true"></span> {{ $category->name }}
            </a>
        @endforeach


          <a target="_blank" title="Formulario de contacto" class="linktitulo" id="BtnMensaje">
                  <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
                  Contacto</a>
   </div>


  </nav>