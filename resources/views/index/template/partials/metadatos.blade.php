<!--SEO-->
<meta http-equiv="content-type" content="text/html;" charset="utf-8"  />
<meta name="title" content="euryrodriguez.com" />
<meta name="author" content="Eury Rodríguez" charset="utf-8" />
<meta name="description" content="descripcion de la pagina" />
<meta name="viewport" content="width-device-width, initial-scale=1, maximum-scale=1"/>
<meta name="viewport" content="initial-scale=1, maximum-scale=1"/>
<meta name="robots" content="index, follow" />
<meta name="abstract" content="Qué esperas para aprender"/>
<meta name="copyright" content="2015"/>

<meta name="rating" content="General"/>

<meta name="creation_Date" content="01/01/2015"/>

<meta name="doc-rights" content="Copywritten work"/>

<meta name="locality" content="Santo Domingo,Republica Domincana"/>

<link rel="canonical" href="http://www.euryrodriguez.com/" />

<link rel="image_src" href="{{ asset('img/dev.png') }}"/>

<meta property="og:title" content="Eury Rodríguez"/>

<meta property="og:type" content="website"/>

<meta property="og:locale" content="es_ES"/>

<meta property="og:url" content="http://www.euryrodriguez.com/"/>

<meta property="og:description" content=""/>

<meta property="og:image" content="{{ asset('img/dev.png') }}"/>

<meta property="og:site_name" content="www.euryrodriguez.com"/>

<meta property="article:publisher" content="https://www.facebook.com/euryrodriguez08"/>

<meta name="twitter:card" content="summary_large_image"/>

<meta name="twitter:site" content="@eury08"/>

<meta name="twitter:title" content="Eury Rodriguez"/>

<meta name="twitter:description" content=""/>

<meta name="twitter:creator" content="@eury08"/>

<meta name="twitter:card" content="summary"/>

<meta name="twitter:image:src" content="{{ asset('img/dev.png') }}"/>

<meta name="twitter:domain" content="www.euryrodriguez.com"/>

<!--SEO-->

<!--<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>-->
@include('index.template.partials.cdn')

<!--CSS-->

        <link href="{{ asset('plugins/Jquery-UI/jquery-ui.min.css')}}" rel="stylesheet" />
        <link rel="stylesheet" type="text/css" href="{{ asset('css/normalizar.css')}}"/>
        <link rel="stylesheet" type="text/css" href="{{ asset('css/estilos.css')}}" />

<!--CSS-->


<link href="{{ asset('favicon.ico')}}" type="image/x-icon" rel="shortcut icon"/>

<!--**********************************************************************************************-->
<style>

    body{
        cursor: url({{ asset('plugins/mouse/left_ptr.cur') }}),auto !important;

    }
    body a:hover
    {
        cursor: url({{ asset('plugins/mouse/pointing_hand.cur') }}),pointer !important;

    }

    #Textbuscar
    {

        cursor:url({{ asset('plugins/mouse/Text.cur') }}), text !important;
    }
    #boton_buscar
    {
        cursor: url({{ asset('plugins/mouse/pointing_hand.cur') }}),pointer !important;

    }

</style>