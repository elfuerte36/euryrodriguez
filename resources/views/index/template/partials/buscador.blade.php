<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<div id="div_form_buscar">

    <!--Buscador de Artícles-->
    {!! Form::open(['route'=>'front.index','method' => 'GET','class'=>'navbar-form pull-right','id'=> 'Form_buscar']) !!}
    <br>
    <div class="row">
        <div class="col-lg-12">
            <div class="input-group">
                <input type="text" name="title" id="Textbuscar" class="form-control Textbuscar" placeholder="Buscar Artículos...">
                            <span class="input-group-btn">
                               <button class="btn btn-warning" type="button" id="btnbusqueda">
                                <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                               </button>
                            </span>
                <div id="resultadob"></div>
            </div><!-- /input-group -->
        </div><!-- /.col-lg-6 -->
    </div><!-- /.row -->
    {!! Form::close() !!}
            <!--Fin del buscador-->

</div>