@extends('index.main')
@section('title','Eury Dariel Pe&ntilde;a Rodr&iacute;guez')
@section('phrases')
    {{ $frase->phrase }}
    <a class="linktitulo" title="Click para ver la biografía de: {{ $frase->author }}" href="{{ $frase->biography }}" >{{ $frase->author }} </a>
@endsection
@section('content')
    @include('index.template.partials.mailform')
    <div id="post">
    <div class="tituloNoticia"><strong>{{ $Article->title }}</strong></div>
        <br>
       <div class="img_articles">
        @foreach($Article->images as $Image)
            <img src="{{ asset('uploads/images/'.$Image->name) }}" alt="" class="img-responsive thumbnail"/>
        @endforeach
       </div>
    <div id="content_detalle" class="wrapword">
        {!!   $Article->content !!}
    </div>

    <div class="meta_datos_post">

        <h4><span class="label label-success"><span class="glyphicon glyphicon-th-list" aria-hidden="true" style="font-weight: bold"> Categor&iacute;a:</span></span> <span class="label label-default">{{ $Article->Category->name }}</span></h4>

        <h4><span class="label label-success"><span class="glyphicon glyphicon-user" aria-hidden="true" style="font-weight: bold"> Autor: </span></span> <span class="label label-default">{{ $Article->User->name }}</span></h4>

        <h4><span class="label label-success"><span class="glyphicon glyphicon-calendar" aria-hidden="true" style="font-weight: bold"> Fecha: </span></span> <span class="label label-default">{{ $Article->created_at->diffForHumans() }} </span></h4>

    </div>

    <br />

    <div class="sep2" style="text-align: center;color:#fff;">
        <h3 class="text-center" style="padding-top: 5px">
            Comentarios <span class="glyphicon glyphicon-comment"></span></h3>
    </div>
    <div id="disqus_thread"></div>
    <script>

        /**
         *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
         *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables */
        /*
         var disqus_config = function () {
         this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
         this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
         };
         */
        (function() { // DON'T EDIT BELOW THIS LINE
            var d = document, s = d.createElement('script');
            s.src = '//euryrodriguez-1.disqus.com/embed.js';
            s.setAttribute('data-timestamp', +new Date());
            (d.head || d.body).appendChild(s);
        })();
    </script>
    <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
    </div>
@endsection
