@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-primary">
                <div class="panel-heading">Bienvenido &nbsp; <strong>{{ Auth::user()->name }}</strong></div>

                <div class="panel-body">
                <div id="divestadisticas">
                  <h3>Estad&iacute;sticas</h3>
                         <hr>
                    <h5>  &nbsp; M&iacute;s art&iacute;culos  <span class="label label-primary"> {{ $Articles->count() }} </span> </h5>
                         <hr>
                    <h5>  &nbsp; Cantidad de usuarios   <span class="label label-primary"> {{ $Users->count() }}    </span> </h5>
                         <hr>
                    <h5>  &nbsp; Cantidad de categorias <span class="label label-primary"> {{ $Categories->count() }}  </span></h5>
                         <hr>
                    <h5>  &nbsp; Cantidad de etiquetas  <span class="label label-primary"> {{ $Tags->count() }} </span> </h5>
                         <hr>
                    <h5>  &nbsp; Cantidad de im&aacute;genes <span class="label label-primary"> {{ $Images->count() }} </span> </h5>
                         <hr>
                    <h5>  &nbsp; Cantidad de frases    <span class="label label-primary"> {{ $Phrases->count() }}  </span> </h5>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
    #divestadisticas span
    {
        float: right;
    }
</style>
@endsection