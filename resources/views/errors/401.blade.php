<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    @include('index.template.partials.metadatos')
    <title>Acceso Restringido</title>
</head>
<body>
<section id="main">
    <a href="{{ URL::to('/')}}">
        <img src="{{ asset('img/dev.png') }}" width="70" height="70" id="logo" />
    </a>
    @include('index.template.partials.header')
    <div id="subir">
        <a href="#cabecera"><img src="{{ asset('img/flecha-arriba (1).png') }}" width="44" height="44" /></a>
    </div>
    <div id="contenido">
        <div class="panel panel-danger">
            <div class="panel-heading"><strong>Acceso Restringido</strong></div>
            <div class="panel-body">
               <img src="{{ asset('img/acceso-restringido.jpg') }}" class="img-responsive img-thumbnail"/>
            </div>
            <div class="panel-footer">
               <strong>Usted no tiene acceso a esta pagína. </strong> <a href="{{ URL::to('/') }}/admin/home" style="color: #fff;" class="btn btn-primary">¿Deseas volver al inicio? <span class="glyphicon glyphicon-home"></span></a>
            </div>
        </div>
    </div>
</section>
@include('index.template.partials.footer')
</body>
</html>