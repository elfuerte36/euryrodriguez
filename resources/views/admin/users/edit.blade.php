@extends('admin.template.main')
@section('title'){{ 'Editar Usuario '.$user->name }} @endsection
@section('titlehead'){{ 'Editar usuario &nbsp;|&nbsp;&nbsp;'.$user->name }} @endsection
@section('content')

{!! Form::open(['route'=>['admin.users.update',$user],'method'=>'PUT']) !!}

		<div class="form-group">
		{!! Form::label('name','Nombre') !!}
		{!! Form::text('name',$user->name,['class'=>'form-control','placeholder'=>'Nombre Completo','required']) !!}
		</div>

		<div class="form-group">
		{!! Form::label('email','Correo Electronico') !!}
		{!! Form::email('email',$user->email,['class'=>'form-control','placeholder'=>'example@gmail.com','required'])!!}
		</div>

		<div class="form-group">
		{!! Form::label('Type','Tipo') !!}
		{!! Form::select('type',[''=>'Seleccione un Nivel','member'=>'Miembro','Admin'=>'Administrador'],$user->type,['class'=>'form-control','required']) !!}
		</div>

		<div class="form-group">
			{!! Form::submit('Registrar',['class'=>'btn btn-primary']) !!}
		</div>

{!! Form::close() !!}

<div style="height:45px"></div>
@endsection