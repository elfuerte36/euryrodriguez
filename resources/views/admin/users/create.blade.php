@extends('admin.template.main')
@section('title'){{ 'Crear Usuario' }}     @endsection
@section('titlehead'){{ 'Crear Usuario' }} @endsection
@section('content')


{!! Form::open(['route'=>'admin.users.store','method'=>'POST']) !!}

		<div class="form-group">
		{!! Form::label('name','Nombre') !!}
		{!! Form::text('name',old('name'),['class'=>'form-control','placeholder'=>'Nombre Completo','required']) !!}
		</div>

		<div class="form-group">
		{!! Form::label('email','Correo Electronico') !!}
		{!! Form::email('email',old('email'),['class'=>'form-control','placeholder'=>'example@gmail.com','required'])!!}
		</div>
	    
	    <div class="form-group">
		{!! Form::label('password','Contraseña') !!}
		{!! Form::password('password',['class'=>'form-control','placeholder'=>'**********************','required'])!!}
		</div>

		<div class="form-group">
		{!! Form::label('Type','Tipo') !!}
		{!! Form::select('type',[''=>'Seleccione un Nivel','member'=>'Miembro','Admin'=>'Administrador'],null,['class'=>'form-control','required']) !!}
		</div>

		<div class="form-group">
			{!! Form::submit('Registrar',['class'=>'btn btn-primary']) !!}
		</div>

{!! Form::close() !!}
<div style="height:45px"></div>
@endsection
