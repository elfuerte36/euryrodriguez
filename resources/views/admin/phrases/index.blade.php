@extends('admin.template.main')
@section('title','Listado de frases')
@section('titlehead','Listado de frases')

@section('content')
    <a href="{{ route('admin.phrases.create') }}" class='btn btn-primary'>Reg&iacute;strar una nueva frase <span class="glyphicon glyphicon-plus"></span></a>
    <hr>

    <table class="table table-striped">

        <thead>
        <th>ID</th>
        <th>Frase</th>
        <th>Autor</th>
        <th>Biograf&iacute;a</th>
        <th>Acci&oacute;n</th>
        </thead>
        <tbody>

        @foreach($Phrases as $Phrase)

            <tr>
                <td>{{ $Phrase->id   }}</td>
                <td>{{ substr($Phrase->phrase,0,50) }}</td>
                <td>{{ $Phrase->author }}</td>
                <td><a href="{{ $Phrase->biography }}" target="_blank">{{ $Phrase->biography }}</a></td>
                <td>

                    <a href="{{ route('admin.phrases.edit',$Phrase->id) }}" class="btn btn-warning"><span class="glyphicon glyphicon-wrench" aria-hidden="true"></span></a>

                    <a href="{{ route('admin.phrases.destroy',$Phrase->id) }}" class="btn btn-danger" onclick="return confirm('Seguro que deseas eliminarlo?')"><span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span></a>

                </td>
            </tr>
        @endforeach

        </tbody>
    </table>

    <div class="text-center">
        {!!  $Phrases->render() !!}
    </div>

@endsection