@extends('admin.template.main')
@section('title','Agregar una frase')
@section('titlehead','Agregar una frase')
@section('content')

    {!! Form::open(['route'=>'admin.phrases.store','method' => 'POST']) !!}

    <div class="form-group">

        {!! Form::label('frase','Frase') !!}
        {!! Form::textarea('phrase',null,['class'=>'form-control','placeholder'=>'Escribe la frase aqu&iacute;','required','style'=>'height:80px']) !!}

    </div>

    <div class="form-group">

        {!! Form::label('author','Autor') !!}
        {!! Form::text('author',null,['class'=>'form-control','placeholder'=>'Escribe el nombre del autor de la frase','required']) !!}

    </div>

    <div class="form-group">

        {!! Form::label('biografia','Enlace de la biograf&iacute;a') !!}
        {!! Form::text('biography',null,['class'=>'form-control','placeholder'=>'Ejemplo: https://es.wikipedia.org/wiki/Albert_Einstein','required']) !!}

    </div>

    {!! Form::submit('Registrar',['class'=>'btn btn-primary']) !!}

    {!! Form::close() !!}

@endsection