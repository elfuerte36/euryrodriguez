@extends('admin.template.main')
@section('title','Editar una frase de '.$Phrase->author)
@section('titlehead','Editar una frase de '.$Phrase->author)
@section('content')

    {!! Form::open(['route'=>['admin.phrases.update',$Phrase],'method' => 'PUT']) !!}

    <div class="form-group">

        {!! Form::label('frase','Frase') !!}
        {!! Form::textarea('phrase',$Phrase->phrase,['class'=>'form-control','placeholder'=>'Escribe la frase aqu&iacute;','required','style'=>'height:80px']) !!}

    </div>

    <div class="form-group">

        {!! Form::label('author','Autor') !!}
        {!! Form::text('author',$Phrase->author,['class'=>'form-control','placeholder'=>'Escribe el nombre del autor de la frase','required']) !!}

    </div>

    <div class="form-group">

        {!! Form::label('biografia','Enlace de la biograf&iacute;a') !!}
        {!! Form::text('biography',$Phrase->biography,['class'=>'form-control','placeholder'=>'Ejemplo: https://es.wikipedia.org/wiki/Albert_Einstein','required']) !!}

    </div>

    {!! Form::submit('Guardar',['class'=>'btn btn-primary']) !!}

    {!! Form::close() !!}

@endsection