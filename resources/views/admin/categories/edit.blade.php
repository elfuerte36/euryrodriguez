@extends('admin.template.main')

@section('title','Editar Categoria '.$category->name)
@section('titlehead','Editar Categoria '.$category->name)

@section('content')


{!! Form::open(['route'=>['admin.categories.update',$category],'method' => 'PUT']) !!}

<div class="form-group">

{!! Form::label('name','Nombre') !!}
{!! Form::text('name',$category->name,['class'=>'form-control','placeholder'=>'Nombre de la categoria','required']) !!}


</div>


{!! Form::submit('Guardar',['class'=>'btn btn-primary']) !!}


{!! Form::close() !!}

@endsection