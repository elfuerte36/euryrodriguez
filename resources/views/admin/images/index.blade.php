@extends('admin.template.main')
@section('title','Listado de imagenes')
@section('titlehead','Listado de imagenes')

@section('content')

 <div class="row">

        @foreach($images as $image)

            <div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-body">
                        <img src="{{ asset('uploads/images/'.$image->name) }}" class="img-responsive img-thumbnail"  style="height: 200px !important;"/>
                    </div>
                    <div class="panel-footer" style="font-style: italic;font-weight: bold">{{ $image->article->title }}</div>
                </div>
            </div>

        @endforeach
 </div>

<div class="text-center">
{!! $images->render() !!}
</div>

@endsection