@extends('admin.template.main')
@section('title'){{ 'Editar Articulo | '.$article->title }}     @endsection
@section('titlehead'){{ 'Editar Articulo | '.$article->title }} @endsection
@section('content')
@include('admin.articles.metadatos')
        <!--CK editor-->
<script src="{{ asset('plugins/ckeditor/ckeditor.js') }}" type="text/javascript" charset="utf-8"></script>

{!! Form::open(['route'=>['admin.articles.update',$article],'method'=>'PUT']) !!}

<div class="form-group">
    {!! Form::label('name','T&iacute;tulo') !!}
    {!! Form::text('title',$article->title,['class'=>'form-control','placeholder'=>'T&iacute;tulo del articulo','required','id'=>'titlearticle']) !!}
</div>


<div class="form-group">
    {!! Form::label('content','Contenido') !!}
    {!! Form::textarea('content',$article->content,['class'=>'form-control','placeholder'=>'Contenido del articulo','required','id'=>'content']) !!}
</div>

<script>
    CKEDITOR.replace( 'content' );
</script>

<div class="form-group">
    {!! Form::label('category_id','Categor&iacute;a') !!}
    {!! Form::select('category_id',$categories,$article->category->id,['class'=>'form-control select-category','required','size'=>'3','style'=>'font-size:16px']) !!}
</div>

<div class="form-group">
    {!! Form::label('tags','Tags') !!}
    {!! Form::select('tags[]',$tags,$my_tags,['class'=>'form-control select-tag','required','multiple','style'=>'font-size:16px']) !!}
</div>

<div class="form-group">

    <button type="submit" class="btn bg-primary" id="sendbutton">
       Guardar Cambios
        <span class="glyphicon glyphicon-ok" aria-hidden="true"></span></button>

</div>

{!! Form::close() !!}

<div style="height:45px"></div>
@endsection
@section('js')

    <script>
        /*******************************************************************/
        $('.select-tag').chosen({
            placeholder_text_multiple:'Seleccione un máximo de 4 tags...',
            max_selected_options:4,
            no_results_text: "No obtuvo ning&uacute;n resultado"
        });
        /****************************************************************/
        $('.select-category').chosen({
            placeholder_text_single:'Seleccione una opción...'
        });
        /*****************************************************************/

    </script>

@endsection