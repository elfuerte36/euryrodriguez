@extends('admin.template.main')
@section('title','Listado de Art&iacute;culos')
@section('titlehead','Listado de Art&iacute;culos')

@section('content')
    <a href="{{ route('admin.articles.create') }}" class='btn btn-primary'>Reg&iacute;strar una nuevo art&iacute;culo <span class="glyphicon glyphicon-plus"></span></a>
    <!--Buscador de Artícles-->
    {!! Form::open(['route'=>'admin.articles.index','method' => 'GET','class'=>'navbar-form pull-right']) !!}
    <br>
    <div class="row">
        <div class="col-lg-12">
            <div class="input-group">
                <input type="text" name="title" class="form-control" placeholder="Buscar Artículo...">
                            <span class="input-group-btn">
                               <button class="btn btn-default" type="submit">
                                   <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                               </button>
                            </span>
            </div><!-- /input-group -->
        </div><!-- /.col-lg-6 -->
    </div><!-- /.row -->
    {!! Form::close() !!}
            <!--Fin del buscador-->
    <hr>

    <table class="table table-striped">

        <thead>
        <th>ID</th>
        <th>T&iacute;tulo</th>
        <th>Categor&iacute;a</th>
        <th>Autor</th>
        <th>Acci&oacute;n</th>
        </thead>
        <tbody>

        @foreach($Articles as $Article)

            <tr>
                <td>{{ $Article->id   }}</td>
                <td>{{ $Article->title  }}</td>
                <td>{{ $Article->category->name }}</td>
                <td>{{ $Article->user->name }}</td>
                <td>

                    <a href="{{ route('admin.articles.edit',$Article->id) }}" class="btn btn-warning"><span class="glyphicon glyphicon-wrench" aria-hidden="true"></span></a>

                    <a href="{{ route('admin.articles.destroy',$Article->id) }}" class="btn btn-danger" onclick="return confirm('Seguro que deseas eliminarlo?')"><span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span></a>

                </td>
            </tr>

        @endforeach

        </tbody>
    </table>

    <div class="text-center">
        {!!  $Articles->render() !!}
    </div>

@endsection