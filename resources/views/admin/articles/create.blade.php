@extends('admin.template.main')
@section('title'){{ 'Crear Articulo' }}     @endsection
@section('titlehead'){{ 'Crear Articulo' }} @endsection
@section('content')
@include('admin.articles.metadatos')
<!--CK editor-->
<script src="{{ asset('plugins/ckeditor/ckeditor.js') }}" type="text/javascript" charset="utf-8"></script>

    {!! Form::open(['route'=>'admin.articles.store','method'=>'POST','files'=>true]) !!}

    <div class="form-group">
        {!! Form::label('name','T&iacute;tulo') !!}
        {!! Form::text('title',old('title'),['class'=>'form-control','placeholder'=>'T&iacute;tulo del articulo','required','id'=>'titlearticle']) !!}
    </div>


    <div class="form-group">
        {!! Form::label('content','Contenido') !!}
        {!! Form::textarea('content',old('content'),['class'=>'form-control','placeholder'=>'Contenido del articulo','required','id'=>'content']) !!}
    </div>

<script>
    CKEDITOR.replace( 'content' );
</script>

    <div class="form-group">
        {!! Form::label('category_id','Categor&iacute;a') !!}
        {!! Form::select('category_id',$Categories,null,['class'=>'form-control select-category','required','size'=>'3','style'=>'font-size:16px']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('tags','Tags') !!}
        {!! Form::select('tags[]',$Tags,null,['class'=>'form-control select-tag','required','multiple','style'=>'font-size:16px']) !!}
    </div>

    <div class="form-group">
            <div id="image-preview">

                    <h3>Portada del art&iacute;culo:</h3>

                    <br />

                        <div id="Div_editar">


                        </div>

                        <div id="file_container">
                            <input type="file" name="image" id="image-upload" />
                        </div>


                    <br />

            </div>

    </div>

    <div class="form-group">

        <button type="submit" class="btn bg-primary" id="sendbutton">
         Publicar Artículo
        <span class="glyphicon glyphicon-ok" aria-hidden="true"></span></button>

    </div>

    {!! Form::close() !!}

    <div style="height:45px"></div>
@endsection
@section('js')
    <script src="{{ asset('plugins/jquery_upload_preview/jquery.uploadPreview.min.js') }}"></script>

    <script>
        /*******************************************************************/
             $('.select-tag').chosen({
               placeholder_text_multiple:'Seleccione un máximo de 4 tags...',
               max_selected_options:4,
               no_results_text: "No obtuvo ning&uacute;n resultado"
           });
         /****************************************************************/
             $('.select-category').chosen({
                 placeholder_text_single:'Seleccione una opción...'
             });
         /*****************************************************************/
             $(document).ready(function() {
                 $.uploadPreview({
                     input_field: "#image-upload",   // Default: .image-upload
                     preview_box: "#file_container",  // Default: .image-preview
                     label_field: "#image-label",    // Default: .image-label
                     label_default: "Choose File",   // Default: Choose File
                     label_selected: "Change File",  // Default: Change File
                     no_label: true                 // Default: false
                 });
             });
        /*******************************************************************/
        </script>
@endsection