<style>

    #image-preview
    {
        width: 250px;
        height:200px;
        margin: auto;

    }
    #file_container
    {

        background-image: url('{{ asset('img/Images-icon.png') }}');
        cursor: pointer;
        width: 120px;
        height: 120px;
        margin-top: -20px;
        margin-left:40px;
        border: 1px solid rgba(192, 192, 192, 1);
    }

    #image-upload
    {
        border:none;
        border: 1px solid rgba(192, 192, 192, 1);
        width:100px !important;
        height:100px !important;
        margin: 5px auto auto 10px;
        opacity: 0;
        cursor: pointer;

    }
    #titlearticle
    {
        font-size: 20px;
        color: #dd1a1a;
        height:45px;
    }
  #sendbutton:hover
  {
      color: #fff;
  }
</style>