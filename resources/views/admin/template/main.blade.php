<!DOCTYPE html>
<html lang="es">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>@yield('title','Default') | Panel de administraci&oacute;n</title>
    @include('admin.template.partials.metadatos')
	<link rel="stylesheet" href="{{ asset('plugins/chosen/chosen.css') }}">
    <link href="{{ asset('favicon.ico')}}" type="image/x-icon" rel="shortcut icon"/>	

</head>
<body>

  @include('admin.template.partials.nav')

	<div class="row-fluid" id="sectionadmin">
		<div class="container">
			
			<div class="panel panel-primary">
				  <div class="panel-heading">
				     <h5 class="panel-title"><b>@yield('titlehead')</b></h5>
				  </div>
				     <div class="panel-body">
					 @include('admin.template.partials.errors')
				     @include('flash::message')
					 @yield('content')
				  </div>
			</div>

		</div>
	</div>

     <footer>
    	@include('admin.template.partials.footer')
    </footer>


<script src="{{ asset('plugins/chosen/chosen.jquery.min.js') }}"></script>
  @yield('js')
</body>
</html>