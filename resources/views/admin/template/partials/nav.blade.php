<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="{{ URL::to('/') }}" target="_blank">Eury Rodr&iacute;guez</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="{{ route('admin.home.index') }}">Inicio <span class="glyphicon glyphicon-home"></span></a></li>
       @if(Auth::user()->admin())
        <li><a href="{{ route('admin.users.index') }}">Usuarios <i class="fa fa-users fa-lg" aria-hidden="true"></i> </a></li>
       @endif
        <li><a href="{{ route('admin.categories.index') }}">Categor&iacute;as <span class="glyphicon glyphicon-th-list"></span></a></li>
        <li><a href="{{ route('admin.articles.index') }}">Artículos <span class="glyphicon glyphicon-pencil"></span></a></li>
        <li><a href="{{ route('admin.images.index') }}">Imágenes <span class=" glyphicon glyphicon-picture"></span></a></li>
        <li><a href="{{ route('admin.tags.index') }}">Etiquetas <span class="glyphicon glyphicon-tags"></span></a></li>
        <li><a href="{{ route('admin.phrases.index') }} ">Frases <i class="fa fa-quote-right fa-lg" aria-hidden="true"></i></a> </li>
      </ul>
      
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" style="color: #000;font-weight: bold" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->name }} &nbsp;<span class="glyphicon glyphicon-user"></span><span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="{{ url('admin/logout') }}"><i class="fa fa-btn fa-sign-out"></i>
                Cerrar Sesi&oacute;n</a></li>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>