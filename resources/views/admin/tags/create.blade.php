@extends('admin.template.main')
@section('title','Agregar Etiqueta')
@section('titlehead','Agregar Etiqueta')

@section('content')
    {!! Form::open(['route'=>'admin.tags.store','method' => 'POST']) !!}
    <div class="form-group">

        {!! Form::label('name','Nombre') !!}
        {!! Form::text('name',old('name'),['class'=>'form-control','placeholder'=>'Nombre de la etiqueta','required']) !!}
    </div>
    {!! Form::submit('Registrar',['class'=>'btn btn-primary']) !!}
    {!! Form::close() !!}

@endsection