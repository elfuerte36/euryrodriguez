@extends("admin.template.main")
@section("title","Lista de etiquetas")
@section('titlehead','Lista de etiquetas')
@section("content")
    <a href="{{ route('admin.tags.create') }}" class="btn btn-primary">Registrar un nueva etiqueta <span class="glyphicon glyphicon-plus"></span></a>
<!--Buscador de Tags-->
    {!! Form::open(['route'=>'admin.tags.index','method' => 'GET','class'=>'navbar-form pull-right']) !!}
<br>
    <div class="row">
            <div class="col-lg-12">
                    <div class="input-group">
                            <input type="text" name="name" class="form-control" placeholder="Buscar etiqueta...">
                            <span class="input-group-btn">
                               <button class="btn btn-default" type="submit">
                                   <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                               </button>
                            </span>
                    </div><!-- /input-group -->
            </div><!-- /.col-lg-6 -->
    </div><!-- /.row -->
    {!! Form::close() !!}
<!--Fin del buscador-->
    <hr>
    <table class="table table-striped">
        <thead>
        <th>ID</th>
        <th>Nombre</th>
        <th>Acci&oacute;n</th>
        </thead>
        <tbody>
        @foreach($Tag as $t)
            <tr>
                <td>{{ $t->id }}</td>
                <td>{{ $t->name }}</td>
           <td>

                    <a href="{{ route('admin.tags.edit',$t->id) }}" class="btn btn-warning"><span class="glyphicon glyphicon-wrench" aria-hidden="true"></span></a>

                    <a href="{{ route('admin.tags.destroy',$t->id) }}" class="btn btn-danger"
                       onclick="return confirm('Seguro que deseas eliminarlo?')">
                        <span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span></a>

                </td>
            </tr>

        @endforeach
        </tbody>
    </table>
<div class="text-center">
    {!! $Tag->render() !!}
</div>
@endsection
