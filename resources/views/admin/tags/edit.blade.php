@extends('admin.template.main')
@section('title','Editar Etiqueta '.$Tag->name)
@section('titlehead','Editar Etiqueta '.$Tag->name)
@section('content')

    {!! Form::open(['route'=>['admin.tags.update',$Tag],'method' => 'PUT']) !!}

    <div class="form-group">
        {!! Form::label('name','Nombre') !!}
        {!! Form::text('name',$Tag->name,['class'=>'form-control','placeholder'=>'Nombre de la etiqueta','required']) !!}
    </div>

    {!! Form::submit('Guardar',['class'=>'btn btn-primary']) !!}
    {!! Form::close() !!}

@endsection