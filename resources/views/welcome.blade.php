@extends('index.main')
@section('title','Padevelopers')
@section('phrases')
{{ $frase->phrase }}
<a class="linktitulo Comic_Sans" title="Click para ver la biografía de: {{ $frase->author }}" href="{{ $frase->biography }}" >{{ $frase->author }} </a>
@endsection
@section('content')
    <div class="contenedor">

@include('index.template.partials.mailform')
        <br />
@if(isset($_GET['title']) && !empty($_GET['title']))
            <div style="width:100%"><h2>Resultados de <strong>'{{ $_GET['title'] }}'</strong></h2></div>
@else
            <div style="width:100%"><h2 style="text-align: center;">Últimas Publicaciones</h2></div>
@endif

        <br />

            <div class="contenedor">

                @foreach($Articles as $Article)

                <article>

    <a href="{{ route('index.view.article',['category' => $Article->category->name,'slug' => $Article->slug]) }}" title="{{ $Article->title }}" class="linktitulo Comic_Sans">
                    <div class="articulos">

                        <h5 class="titulo" style="color: #337ab7 !important;font-size: 13px;">
                         {!! substr($Article->title,0,35) !!}...
                        </h5>


                        <br />
                        @foreach($Article->images as $Image)
                        <img src="{{ asset('uploads/images/'.$Image->name) }}" width="100" height="100" alt="" />
                        @endforeach

                        <br>
                        <br>

                        <span class="label label-primary por"><span class="glyphicon glyphicon-user"></span>&nbsp; Eury Rodriguez </span>

                        <br />
                        <span class="label label-success cat">
                        <span class="glyphicon glyphicon-th-list"></span> &nbsp;{{ $Article->category->name }}
                        </span>

                        <br>
                        <span class="fecha">{{ $Article->created_at->diffForHumans() }}</span>
                        <br />
                        <br>
                    </div>
    </a>
                 </article>

                @endforeach

            </div>


    </div>

    <div class="text-center">
        {!! $Articles->render() !!}
    </div>

    <hr />
    @include('index.template.partials.categories')
@endsection
